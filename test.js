// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var getCookie = require('./index.js');
    var assert = require('assert');

    describe('get-cookie', function () {
        it('should get the only cookie', function () {
            var cookies = ['my_cookie=test'];

            global.window = {
                decodeURIComponent: decodeURIComponent,
                document: {
                    cookie: cookies.join('; ')
                }
            };

            var targetCookie = cookies[0].split('=');
            assert.equal(getCookie(targetCookie[0]), targetCookie[1]);
        });

        it('should get the first cookie of 3', function () {
            var cookies = [
                'my_cookie=test',
                'my_other_cookie=test',
                'a_final-cookie=super_good_cookie'
            ];

            global.window = {
                decodeURIComponent: decodeURIComponent,
                document: {
                    cookie: cookies.join('; ')
                }
            };

            var targetCookie = cookies[0].split('=');
            assert.equal(getCookie(targetCookie[0]), targetCookie[1]);
        });

        it('should get the last cookie of 3', function () {
            var cookies = [
                'my_cookie=test',
                'my_other_cookie=test',
                'a_final-cookie=super_good_cookie'
            ];

            global.window = {
                decodeURIComponent: decodeURIComponent,
                document: {
                    cookie: cookies.join('; ')
                }
            };

            var targetCookie = cookies[2].split('=');
            assert.equal(getCookie(targetCookie[0]), targetCookie[1]);
        });

        it('should get the last cookie of 3 even when last character is a semi-colon', function () {
            var cookies = [
                'my_cookie=test',
                'my_other_cookie=test',
                'a_final-cookie=super_good_cookie'
            ];

            global.window = {
                decodeURIComponent: decodeURIComponent,
                document: {
                    cookie: cookies.join('; ') + ';'
                }
            };

            var targetCookie = cookies[2].split('=');
            assert.equal(getCookie(targetCookie[0]), targetCookie[1]);
        });

        it('should decode cookie values', function () {
            var cookies = [
                'my_cookie=test',
                'my_other_cookie=test',
                'a_final-cookie=test_value%24for%25!%3D'
            ];

            global.window = {
                decodeURIComponent: decodeURIComponent,
                document: {
                    cookie: cookies.join('; ') + ';'
                }
            };

            var targetCookie = cookies[2].split('=');
            assert.equal(getCookie(targetCookie[0]), 'test_value$for%!=');
        });

        it('should be able to have a cookie string provided', function () {
            var cookies = 'my_cookie=test; other_cookie=test';
            assert.equal(getCookie('my_cookie', cookies), 'test');
        });
    });
