var iterateUpArray = require('@amphibian/iterate-up-array');

var cookieSeparatorRegExp = /;\s?/;
var STRING_EQUALS = '=';
var decodeUriComponent = decodeURIComponent || window.decodeURIComponent;

/**
 * Get cookie
 * @param {string} name - cookie key name 
 * @param {string} [string] - cookie string
 * @returns {string} value
**/
module.exports = function getCookie(name, string) {
    var cookies = (string || window.document.cookie).split(cookieSeparatorRegExp);
    var match = iterateUpArray(cookies, function (cookie, i, end) {
        var pair = cookie.split(STRING_EQUALS);

        if (pair[0] === name) {
            end(decodeUriComponent(pair[1]));
        }
    });

    return match || false;
}
